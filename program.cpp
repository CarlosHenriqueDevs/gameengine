#include <iostream>
#include "engine/vector3d.h"

using namespace std;

int main()
{
	cout << "hello" << endl;
	vector3d v(10, 10);
	
	cout << "x = " << v.x << endl;
	cout << "y = " << v.y << endl;
	
	cout << endl;
	
	v.set(20, 20);
	
	cout << "x = " << v.x << endl;
	cout << "y = " << v.y << endl;
	
	return 0;
}