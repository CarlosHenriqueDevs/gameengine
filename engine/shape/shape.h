#ifndef SHAPE_H
#define SHAPE_H
#include "../vector3d.h"
#include "color.h"

class shape
{
	private:
	   float width, height, depht;
	   vector3d coords;
	   color shapeColor;
	   
	public:
	   shape(float width, float height, float depht);
	   shape(float width, float height);
	   shape(vector3d coords, float width, float height, float depht);
	   shape(vector3d coords, float width, float height);
	   
	   void setColor(color& c = *color::RED);
	   void setColor(float r, float g, float b, float a);
	   void draw(long fps);
	   void update(long fps);
	   float[] getVertices();
	   void setPosition(float x, float y);
	   void setPosition(float x, float y, float z);
	   void setPosition(vector3d& position);
	   void setSize(float width, float height);
	   bool equals(shape& other);
};

#endif