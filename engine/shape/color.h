#ifndef COLOR_H
#define COLOR_H

class color
{
	private:
	   float r, g, b, a;
	   
	public:
	   static const color RED(1.0f, 0.0f, 0.0f, 0.0f);
	   static const color GREEN(0.0f, 1.0f, 0.0f, 0.0f);
	   static const color BLUE(0.0f, 0.0f, 1.0f, 0.0f);
	   static const color ALPHA(0.0f, 0.0f, 0.0f, 1.0f);
	   
	   color(float r, float g, float b, float a);
	   
	   void set(float r, float g, float b, float a = 0.0f);
	   void setTransparency(float value);
};

#endif