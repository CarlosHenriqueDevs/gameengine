#ifndef VECTOR3D_H
#define VECTOR3D_H

class vector3d
{
	public:
	   float x, y, z;
	   
	   vector3d(float x, float y);
	   vector3d(float x, float y, float z);
	   
	   void set(float x, float y, float z);
	   void set(float x, float y);
	   void set(vector3d& other);
	   vector3d operator+=(vector3d& other);
	   vector3d operator-=(vector3d& other);
	   vector3d operator/=(vector3d& other);
	   vector3d operator*=(vector3d& other);
};

#endif