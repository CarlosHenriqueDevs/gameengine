#include "vector3d.h"

vector3d::vector3d(float x, float y)
{
	this->x = x;
	this->y = y;
}

vector3d::vector3d(float x, float y, float z)
{
	vector3d(x, y);
	this->z = z;
}
	   
void vector3d::set(float x, float y, float z)
{
	this->x = x;
	this->y = y;
	this->z = z;
}

void vector3d::set(float x, float y)
{
	this->x = x;
	this->y = y;
}

void vector3d::set(vector3d& other)
{
	this->x = other.x;
	this->y = other.y;
	this->z = other.z;
}

vector3d vector3d::operator+=(vector3d& other)
{
	
}

vector3d vector3d::operator-=(vector3d& other)
{
	
}

vector3d vector3d::operator/=(vector3d& other)
{
	
}

vector3d vector3d::operator*=(vector3d& other)
{
	
}